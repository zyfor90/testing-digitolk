<?php

namespace DTApi\Http\Controllers;

use DTApi\Models\Job;
use DTApi\Http\Requests;
use DTApi\Models\Distance;
use Illuminate\Http\Request;
use DTApi\Repository\BookingRepository;

/**
 * Class BookingController
 * @package DTApi\Http\Controllers
 */
class BookingController extends Controller
{

    /**
     * @var BookingRepository
     */
    protected $repository;

    /**
     * BookingController constructor.
     * @param BookingRepository $bookingRepository
     */
    public function __construct(BookingRepository $bookingRepository)
    {
        $this->repository = $bookingRepository;
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function index(Request $request)
    {
        $response = ""; 
        $userType = $request->__authenticatedUser->user_type;

        if($request->get('user_id')) {
            $response = $this->repository->getUsersJobs($user_id);
        } elseif($userType == env('ADMIN_ROLE_ID') || $userType == env('SUPERADMIN_ROLE_ID')) {
            $response = $this->repository->getAll($request);
        }

        return response($response);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function show($id)
    {
        return response($this->repository->with('translatorJobRel.user')->find($id));
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function store(Request $request)
    {
        try {
            return response($this->repository->store($request->__authenticatedUser, $request->all()));
        } catch (\Throwable $th) {
            return response($th->getMessage());
        }

    }

    /**
     * @param $id
     * @param Request $request
     * @return mixed
     */
    public function update($id, Request $request)
    {
        try {
            return response($this->repository->updateJob($id, array_except($request->all(), ['_token', 'submit']), $request->__authenticatedUser));
        } catch (\Throwable $th) {
            return response($th->getMessage());
        }
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function immediateJobEmail(Request $request)
    {
        return response($this->repository->storeJobEmail($request->all()));
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function getHistory(Request $request)
    {
        return response($request->get('user_id') ? $this->repository->getUsersJobsHistory($user_id, $request) : "");
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function acceptJob(Request $request)
    {
        return response($this->repository->acceptJob($request->all(), $request->__authenticatedUser));
    }

    public function acceptJobWithId(Request $request)
    {
        return response($this->repository->acceptJobWithId($request->get('job_id'), $request->__authenticatedUser));
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function cancelJob(Request $request)
    {
        return response($this->repository->cancelJobAjax($request->all(), $request->__authenticatedUser));
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function endJob(Request $request)
    {
        return response($this->repository->endJob($request->all()));
    }

    public function customerNotCall(Request $request)
    {
        return response($this->repository->customerNotCall($request->all()));
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function getPotentialJobs(Request $request)
    {
        return response($this->repository->getPotentialJobs($request->__authenticatedUser));
    }

    public function distanceFeed(Request $request)
    {
        $data = $request->all();

        if ($data['flagged'] == 'true' && $data['admincomment'] == '') {
            return "Please, add comment";
        }

        $fieldParams = ['distance', 'time', 'jobid', 'session_time', 'admincomment'];
        foreach ($fieldParams as $fieldParam) {
            ${$fieldParam} = $this->distanceFeedCheckParam($data[$fieldParam]);
        }
        
        $fieldTrueOrFalses = ['flagged', 'manually_handled', 'by_admin'];
        foreach ($fieldTrueOrFalses as $fieldTrueOrFalse) {
            ${$fieldTrueOrFalse} = $this->distanceFeedCheckTrueOrFalse($data[$fieldTrueOrFalse]);
        }
        
        if ($time || $distance) {
            $affectedRows = Distance::where('job_id', '=', $jobid)->update(array('distance' => $distance, 'time' => $time));
        }

        if ($admincomment || $session_time || $flagged || $manually_handled || $by_admin) {
            $affectedRows1 = Job::where('id', '=', $jobid)->update(array('admin_comments' => $admincomment, 'flagged' => $flagged, 'session_time' => $session_time, 'manually_handled' => $manually_handled, 'by_admin' => $by_admin));
        }

        return response('Record updated!');
    }

    public function distanceFeedCheckParam($param) 
    {
        $data = "";
        if (isset($param) && $param != "") {
            $data = $param;
        }

        return $data;
    }

    public function distanceFeedCheckTrueOrFalse($param) 
    {
        if ($param == 'true') { 
            $data = 'yes';
        } else {
            $data = 'no';
        }

        return $data;
    }

    public function reopen(Request $request)
    {
        return response($this->repository->reopen($request->all()));
    }

    public function resendNotifications(Request $request)
    {
        $job = $this->repository->find($request->jobid);
        $this->repository->sendNotificationTranslator($job, $this->repository->jobToData($job), '*');

        return response(['success' => 'Push sent']);
    }

    /**
     * Sends SMS to Translator
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function resendSMSNotifications(Request $request)
    {
        try {
            $this->repository->sendSMSNotificationToTranslator($this->repository->find($request->jobid));
            return response(['success' => 'SMS sent']);
        } catch (\Exception $e) {
            return response(['success' => $e->getMessage()]);
        }
    }

}
