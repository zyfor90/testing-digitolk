<?php

namespace App\Traits;

use DTApi\Mailers\AppMailer;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Monolog\Handler\FirePHPHandler;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ConnectException;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\RuntimeException;
use GuzzleHttp\Exception\ServerException;
use GuzzleHttp\Exception\TooManyRedirectsException;
use GuzzleHttp\Exception\TransferException;


trait Helper
{
    public function sendEmail($email, $name, $subject, $tag, $data)
    {
        $mailer = new AppMailer();
        $mailer->send($email, $name, $subject, $tag, $data);
    }

    public function createLogger($loggerChannel, $loggerFile)
    {
        $logger = new Logger($loggerChannel);
        $logger->pushHandler(new StreamHandler(storage_path($loggerFile), Logger::DEBUG));
        $logger->pushHandler(new FirePHPHandler());
        return $logger;
    }


    public function sendRequest($url, $method, $data, $header)
    {
        try {
            $client = new Client;
            $res = $client->request($method, $url, [
                'headers' => array(
                    'Content-Type' => 'application/json',
                    $header
                ),
                'json' => $data,
            ]);

            $response = json_decode($res->getBody(), true);
            return $response;
        } catch (RuntimeException $e) {
            return $e->getMessage();
        } catch (TransferException $e) {
            return $e->getMessage();
        } catch (ConnectException $e) {
            return $e->getMessage();
        } catch (RequestException $e) {
            return $e->getMessage();
        } catch (BadResponseException $e) {
            return $e->getMessage();
        } catch (ServerException $e) {
            return $e->getMessage();
        } catch (ClientException $e) {
            return $e->getMessage();
        } catch (TooManyRedirectsException $e) {
            return $e->getMessage();
        }
    }

}
