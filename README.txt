
# My thoughts about the code :

1. In the BookingController, I simplified it to one line because assigning a value to a variable before using it can slow down the process. This is because, in PHP, every time you create a variable, a new memory address is allocated. By avoiding the creation of separate variables for $data and $cuser, you can save resources, as you won't need to store the memory addresses of these variables in your RAM.

        $data = $request->all();
        $cuser = $request->__authenticatedUser;
        $response = $this->repository->updateJob($id, array_except($data, ['_token', 'submit']), $cuser);

So, instead of doing this, you should try putting the value into the function parameter like this.

    response = $this->repository->updateJob($id, array_except($request->all(), ['_token', 'submit']),$request->__authenticatedUser);

Except if the variable is used more than once, in which case you can do this. For example, if there is a future change in the value of the $cuser variable, developers only need to modify one base variable.

        $data = $request->all();
        $cuser = $request->__authenticatedUser;
        $response = $this->repository->createJob($id, array_except($data, ['_token', 'submit']), $cuser);
        $response = $this->repository->updateJob($id, array_except($data, ['_token', 'submit']), $cuser);
        $response = $this->repository->deletejob($id, array_except($data, ['_token', 'submit']), $cuser);

2. And after that refactoring, I simplified it by reducing it to just one line, like this.

        return response($this->repository->updateJob($id, array_except($request->all(), ['_token', 'submit']), $request->__authenticatedUser));

3. In the BookingController function distanceFeed, there are many similar logics. Therefore, I decided to create two reusable functions as shown below, and I replaced the identical logic or validator with these functions.
    
        private function distanceFeedCheckParam($param) 
        {
            $data = "";
            if (isset($param) && $param != "") {
                $data = $param;
            }

            return $data;
        }

        private function distanceFeedCheckTrueOrFalse($param) 
        {
            if ($param == 'true') { 
                $data = 'yes';
            } else {
                $data = 'no';
            }

            return $data;
        } 

And it became like this.
        
        $this->distanceFeedCheckParam($data[$fieldParam]);

4. After that, I created an array variable with values like this: ['distance', 'time', 'jobid', 'session_time', 'admincomment']. Then, I looped through it like this:

        $fieldParams = ['distance', 'time', 'jobid', 'session_time', 'admincomment'];
        foreach ($fieldParams as $fieldParam) {
            ${$fieldParam} = $this->distanceFeedCheckParam($data[$fieldParam]);
        }
        
        $fieldTrueOrFalses = ['flagged', 'manually_handled', 'by_admin'];
        foreach ($fieldTrueOrFalses as $fieldTrueOrFalse) {
            ${$fieldTrueOrFalse} = $this->distanceFeedCheckTrueOrFalse($data[$fieldTrueOrFalse]);
        }
    
5. I made various changes to the code in the BookingRepository. However, there are specific cases I'd like to highlight, such as the store function in the booking repository. Previously, it repeatedly used arrays for status, message, and field. To streamline the code, I created a function. An example is as follows:

        if (!isset($data['from_language_id'])) {
            $response['status'] = 'fail';
            $response['message'] = "Du måste fylla in alla fält";
            $response['field_name'] = "from_language_id";
            return $response;
        }
        if ($data['immediate'] == 'no') {
            if (isset($data['due_date']) && $data['due_date'] == '') {
                $response['status'] = 'fail';
                $response['message'] = "Du måste fylla in alla fält";
                $response['field_name'] = "due_date";
                return $response;
            }
            if (isset($data['due_time']) && $data['due_time'] == '') {
                $response['status'] = 'fail';
                $response['message'] = "Du måste fylla in alla fält";
                $response['field_name'] = "due_time";
                return $response;
            }
            if (!isset($data['customer_phone_type']) && !isset($data['customer_physical_type'])) {
                $response['status'] = 'fail';
                $response['message'] = "Du måste göra ett val här";
                $response['field_name'] = "customer_phone_type";
                return $response;
            }
            if (isset($data['duration']) && $data['duration'] == '') {
                $response['status'] = 'fail';
                $response['message'] = "Du måste fylla in alla fält";
                $response['field_name'] = "duration";
                return $response;
            }
        } else {
            if (isset($data['duration']) && $data['duration'] == '') {
                $response['status'] = 'fail';
                $response['message'] = "Du måste fylla in alla fält";
                $response['field_name'] = "duration";
                return $response;
            }
        }

became like this

    if (!isset($data['from_language_id'])) {
        return $this->storeResponse("Du måste fylla in alla fält", "from_language_id");
    } else if ($data['immediate'] == 'no' && isset($data['due_date']) && $data['due_date'] == '') {
        return $this->storeResponse("Du måste fylla in alla fält", "due_date");
    } else if ($data['immediate'] == 'no' && isset($data['due_time']) && $data['due_time'] == '') {
        return $this->storeResponse("Du måste fylla in alla fält", "due_time");
    } else if ($data['immediate'] == 'no' && !isset($data['customer_phone_type']) && !isset($data['customer_physical_type'])) {
        return $this->storeResponse("Du måste göra ett val här", "customer_phone_type");
    } else if ($data['immediate'] == 'no' && isset($data['duration']) && $data['duration'] == '') {
        return $this->storeResponse("Du måste fylla in alla fält", "duration");
    } else if ($data['immediate'] != 'no' && isset($data['duration']) && $data['duration'] == '') {
        return $this->storeResponse("Du måste fylla in alla fält", "duration");
    }

then I add this funciton
    
    private function storeResponse($message, $field = null) 
    {
        return [
            'status' => 'fail',
            'message' => $message,
            'field_name' => $field
        ];
    }

I also removed some unused code in the store function of the BookingRepository. I noticed there was commented-out code after the line where the $job = $cuser->jobs()->create($data); function is called.

        //Event::fire(new JobWasCreated($job, $data, '*'));

        //$this->sendNotificationToSuitableTranslators($job->id, $data, '*');// send Push for New job posting

Because the variable $data is intended to be used by $job = $cuser->jobs()->create($data), there shouldn't be any further calls to the variable $data after the function $job = $cuser->jobs()->create($data). Therefore, I deleted all the lines that call the variable $data. Here is the code I removed:
    
    
        $response['status'] = 'success';
        $response['id'] = $job->id;
        $data['job_for'] = array();
        if ($job->gender != null) {
            if ($job->gender == 'male') {
                $data['job_for'][] = 'Man';
            } else if ($job->gender == 'female') {
                $data['job_for'][] = 'Kvinna';
            }
        }
        if ($job->certified != null) {
            if ($job->certified == 'both') {
                $data['job_for'][] = 'normal';
                $data['job_for'][] = 'certified';
            } else if ($job->certified == 'yes') {
                $data['job_for'][] = 'certified';
            } else {
                $data['job_for'][] = $job->certified;
            }
        }

        $data['customer_town'] = $cuser->userMeta->city;
        $data['customer_type'] = $cuser->userMeta->customer_type;

5. I changed the jobEnd function and added the sendEmail, createLogger, and sendRequest functions in App\Traits\Helper. Afterward, I used the Helper class in the base controller..

# Important Note : 
#### - Firstly, I would like to express my gratitude for allowing me to take this technical test. I want to apologize as I could only complete a few functions. Currently, I am working at a company, and unfortunately, in the past week, I had to work overtime and had limited time to work on this technical test. I have already refactored the BookingController and BookingRepository from LINE 44 to 498 and 1910 to 2076.

#### - If I had more time, I would proceed with the following steps:
1. First, I would replace the variable Request $request using Laravel requests to validate and define parameters along with rules when using that function. The reason is that Laravel does not have a struct like other programming languages, such as Golang. So, if Laravel does not validate and define parameters inside the request variable, there is a possibility for hackers to exploit over-posting methods.
https://learn.microsoft.com/en-us/aspnet/core/data/ef-mvc/crud?view=aspnetcore-8.0#security-note-about-overposting

2. I noticed in the booking repository that there are calls to Eloquent or database connections. Ideally, these queries or Eloquent operations should be moved or placed into their own functions within the BookingRepository. This is because the controller should primarily handle the display of data and validate incoming parameters.

        if ($time || $distance) {
            $affectedRows = Distance::where('job_id', '=', $jobid)->update(array('distance' => $distance, 'time' => $time));
        }

        if ($admincomment || $session_time || $flagged || $manually_handled || $by_admin) {
            $affectedRows1 = Job::where('id', '=', $jobid)->update(array('admin_comments' => $admincomment, 'flagged' => $flagged, 'session_time' => $session_time, 'manually_handled' => $manually_handled, 'by_admin' => $by_admin));
        }
        
3. I will create a new layer, namely the logic/service layer, with the purpose of separating the presentation layer from the logic layer. The controller should primarily validate incoming requests and display responses. The logic layer, or service layer, should handle the business logic, and then there should be a repository layer in the third layer. This separation of concerns helps maintain a cleaner and more organized code structure.